

'''

Ce fichier va charger un fichier contenant sur chaque ligne un fichier à
inclure dans le gros fichier à construire

'''

latex_template = ''
latex_template_head = ''
latex_template_tail = ''

with open('latextemplate-head.tex', 'r', encoding='utf-8') as fd:
	latex_template_head = fd.read()
with open('latextemplate-tail.tex', 'r', encoding='utf-8') as fd:
	latex_template_tail = fd.read()

# with open(choicefile, 'r', encoding='utf-8') as fd:
# 	for file in choicefile:
# 		latex += include(file)

# 	print(latex_template.format(latex=latex))

def include(file, no):
	with open(file, 'r', encoding='utf-8') as fd:
		filecontent = fd.read()
	latex = '''
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Exercice {no}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
'''.format(no=no)
	latex += r'\begin{exo}{' + str(no) + '}\n'
	latex += filecontent
	latex += r'\end{exo}'
	latex += '\n\n'

	return latex

latex = ''
no = 1
while True:
	try:
		file = input()
	except:
		break

	# pour ne pas tenir compte des lignes qui commencent par un # (exercices
	# enlevés par commentaires)
	filename = file.split('#')[0]

	try:
		latex += include(filename, str(no)+' $\longrightarrow$' + file.split('/')[-1])
		no += 1
	except:
		pass

	

print(latex_template_head)
print(latex)
print(latex_template_tail)