import re

'''

Ce fichier lit en entrée un exercice latex au format

\begin{exo}{:int:}
:donnee:
\begin{exoitems}{:n_colonnes:}

\end{exoitems}
\end{exo}

et extrait les items de l'exercice, un par ligne

'''

def get_items(file):
	with open(file, 'r', encoding='utf-8') as fd:
		latex = fd.read()

		# commencer par ne prendre que les items (si il y a un environnement 'exoitems')
		try:
			latex = latex.replace(r'\begin{enumerate}[a)]', r'\begin{exoitems}{1}')
			items_code = re.split(r'\\begin\{exoitems\}\{[0-9]*\}.*\n', latex)[1].split(r'\end{exoitems}')[0]
			items = re.split(r'[\t ]*\\item[\t ]*', items_code)[1:]
			items = [x.strip() for x in items if x != '']
		except:
			items = []

		return items

while True:
	try:
		file = input()
	except:
		break

	# pour ne pas tenir compte des lignes qui commencent par un # (exercices
	# enlevés par commentaires)
	filename = file.split('#')[0]

	try:
		items = get_items(filename)
		if len(items) > 1:
			no = len(items)
		else:
			no = 1

		print(filename, 'donnee')
		for (n, i) in enumerate(items):
			print(filename, chr(ord('a')+n), i)
		
	except:
		pass

	
