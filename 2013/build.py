
import os
import re

'''

Ce fichier va charger un fichier contenant sur chaque ligne un fichier à
inclure dans le gros fichier à construire

'''

class Exo(object):

	def __init__(self, no, unit, file):

		self.no = no
		self.unit = unit
		self.file = file

	def latex(self):
		print('file', self.file)

	def _repr__(self):
		return 'Exo({}, {}, {})'.format(no, unit, file)


class ExoLoader(object):

	def __init__(self, folder):
		'''

		Cette fonction va visiter le dossier ../folder/exos/ et va créer un
		dictionnaire permettant d'associer à chaque fichier présent dans ce
		dossier un numéro d'exercice.

		'''
		self.unit_dict = {}
		self.source_folder = os.path.join('..', folder, 'exos')

		files = os.listdir(self.source_folder)
		pattern = r'ma1ecg-(.*)-exo-(\d*)-no-([0-9a-z]*).tex'

		for file in files:
			groups = re.findall(pattern, file)
			
			(unit, position, no) = tuple(groups[0])
			# unit = file.split('ma1ecg-')[1].split('-donnee-exo-')[0]
			# no = file.split('no-')[1].split('.tex')[0]
			
			self.unit_dict[no] = Exo(no, unit, file)

	def latex(self, no):
		with open(os.path.join(self.source_folder, self.unit_dict[no].file), 'r', encoding='utf-8') as fd:
			latex = fd.read()

			return latex

	def get(self, no):
		return self.unit_dict[no]

	def __repr__(self):
		return str(list(self.unit_dict))

def include(file, no):
	with open(file, 'r', encoding='utf-8') as fd:
		filecontent = fd.read()
	latex = '''
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Exercice {no}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
'''.format(no=no)
	latex += r'\begin{exo}{' + str(no) + '}\n'
	latex += filecontent
	latex += r'\end{exo}'
	latex += '\n\n'

	return latex


def main():

	latex_template = ''
	latex_template_head = ''
	latex_template_tail = ''

	with open('../latextemplate-head.tex', 'r', encoding='utf-8') as fd:
		latex_template_head = fd.read()
	with open('../latextemplate-tail.tex', 'r', encoding='utf-8') as fd:
		latex_template_tail = fd.read()

	# with open(choicefile, 'r', encoding='utf-8') as fd:
	# 	for file in choicefile:
	# 		latex += include(file)

	# 	print(latex_template.format(latex=latex))


	latex = ''
	current_no = 1
	while True:
		try:
			line = input().strip()
		except Exception as e:
			# print("Erreur", e)
			break;

		# pour ne pas tenir compte des lignes qui commencent par un # (exercices
		# enlevés par commentaires)
		#filename = file.split('#')[0]

		groups = re.findall(r'^:include:(.+):([0-9a-z]+)', line)
		# print(groups)

		if len(groups) == 0:
			latex += line + '\n'
		else:
			(unit, no) = tuple(groups[0])

	# 		# pas performant mais je ferai mieux plus tard
			loader = ExoLoader(unit)

			file = os.path.join(loader.source_folder, loader.get(no).file)


			try:
				#latex += include(file, str(current_no)+' $\longrightarrow$' + file.split('/')[-1])
				latex += include(file, str(current_no))
				current_no += 1
			except:
				pass

		

	print(latex_template_head.replace(':title:', "Recueil d'exercices de calcul, 1ECG -- version 2013"))
	print(latex)
	print(latex_template_tail)


if __name__ == '__main__':
	main()