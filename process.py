#!/usr/bin/python3

import re


'''

Expressions régulières utiles

*   Pour remplacer les \begin{exoitems}{1} par des \begin{enumerate} :
    
    \\begin\{exoitems\}\{1\}.*\n((.*\n)*?)\\end\{exoitems\} ==> \\begin\{enumerate\}\n\1\\end\{enumerate\}

'''

def parse(latex):

    matches = re.findall(r'(\\begin\{exo\}\{(?P<no_exo>[0-9a-z]+).*\}(.*\n)*?\\end\{exo\})', latex)

    return matches

def remove_exo_tag(latex):
    result = latex
    result = re.sub(r'\\begin\{exo\}.*\n', '', result)
    result = re.sub(r'\\end\{exo\}.*\n?', '', result)
    result = re.sub(r'\\section\{.*\n?', '', result)
    return result

if __name__ == '__main__':
    inputfile = input("fichier latex à splitter : ")
    # inputfile = 'arithmetique-donnee'
    # inputfile = 'algebre-donnee'
    # inputfile = 'algebre-synthese-donnee'
    # inputfile = 'recueil'
    # inputfile = 'pase-exos'
    print(inputfile)

    with open(inputfile, 'r', encoding='utf-8') as latexfile:
        latex = latexfile.read()
        matches = parse(latex)

    i = 1
    for m in matches:
        # print('match', m)

        (exo_latex, no_exo) = m[:2]
        outfilename = 'exos/ma1ecg-{0}-exo-{1:03}-no-{2}.tex'.format(inputfile.split('.tex')[0], i, no_exo)
        with open(outfilename, 'w', encoding = 'utf-8') as outfile:
            exo_latex = remove_exo_tag(exo_latex)
            print("written", outfilename)
            outfile.write(exo_latex)
        i += 1