build-recueil:
	cat recueil/recueil-choice.txt | python3 recueil/build.py > recueil/recueil-donnee.tex 
	pdflatex recueil/recueil-donnee.tex && mv recueil-donnee* recueil/

split-recueil:
	cd recueil/ && 	rm -f exos/* &&	echo "recueil-donnee.tex" | python3 process.py

build-2013:
	cd 2013/ && cat ordre_exos_2013.tex | python3 build.py > 2013-donnee.tex && cd ..
	pdflatex 2013/2013-donnee.tex
	mv 2013-donnee.* 2013/